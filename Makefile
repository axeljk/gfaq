CXX         = g++
CXXFLAGS    = -Wall -O3 -Wno-ignored-qualifiers
LIBRARIES  := -lifag
SOURCE      = $(wildcard *.cc)
OBJDIR     := obj
OBJECTS    := $(SOURCE:%.cc=$(OBJDIR)/%.o)
EXECUTABLE  = _gfaq

.DELETE_ON_ERROR:
all: $(EXECUTABLE)

$(OBJDIR)/%.o: %.cc
	@echo BARF
	@$(CXX) $(CXXFLAGS) -Llib -c $< -o $@

$(EXECUTABLE): $(OBJECTS)
	@echo PUKE
	@$(CXX) $^ -Llib $(LIBRARIES) -o $(EXECUTABLE)

.PHONY: clean cleanest

clean:
	del $(OBJECTS)

cleanest: clean
	del $(EXECUTABLE)