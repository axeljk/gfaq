#pragma once


#include "ifag/io_file.h"
#include "ifag/blurb.h"
#include "ifag/stat.h"
#include "motive.h"
#include "vm.h"


struct actor;
struct party;

struct battle {
	fag_file *test_file;
	party *player, *enemies;
	motive *motives;
	int motives_count;
	int base_speed;
	inline static const uint32_t kXPAttack	= 0x00100000;
	inline static const uint32_t kXPHit		= 0x00080000;
	inline static const uint32_t kXPMultStr = 0x00000001;
	inline static const uint32_t kXPMultWis = 0x00000001;
	inline static const uint32_t kXPMultAgi = 0x00000005;

	int Attack(motive &m);
	int Battle();
	void Enemies();
	void ActionReport();
	void PlayerInput(actor *p, motive &m);
	double StatPercent(stat_base *a, stat_base *b);
	void LevelStat(const actor &act, const uint32_t &x, stat_aux *a, stat_aux *b);
	void EnCost(actor *a, const uint32_t &cost);
	double XPMultiplier(const int &x);
	void RandomBlurb(motive *m);
	void CheatLevel(actor *a, const int &l);

	battle(party *p, party *e, fag_file *f)
	: test_file(f)
	, player(p)
	, enemies(e)
	, motives_count(0) {
		Battle();
	}

	~battle() {
		delete[] motives;
	}

	private:
		vm v_;
};